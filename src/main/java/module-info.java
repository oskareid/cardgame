module no.ntnu.card {

    requires transitive javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.card.controller to javafx.fxml;

    //opens no.ntnu.card; // Må ha med denne for at maven 'clean, test'
                        // skal få tilgang til test klassene.

    exports no.ntnu.card;

}